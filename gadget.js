var api = "https://www.googleapis.com/fusiontables/v1/";

function callback(response) {
    console.log(JSON.stringify(response));
}

function getTable(table) {
    var url = api + "tables/" + table + "?key=AIzaSyApVcaHU-Drez2RpLdOu5AUOZeL6tOq6Lk";
    
    console.log("GET " + url);
    gadgets.io.makeRequest(url, callback);
}

function makeQuery(query) {
    var sql = encodeURIComponent(query);
    var url = api + "query?sql=" + sql + "&key=AIzaSyApVcaHU-Drez2RpLdOu5AUOZeL6tOq6Lk";
    
    console.log("GET " + url);
    gadgets.io.makeRequest(url, callback);
}

function fetchData() {
    console.log("foo");
    getTable("14LHJw1YN3Q3xShzkAzVveqOX-w-eJoxQfoKklknJ");
    makeQuery("select * from 14LHJw1YN3Q3xShzkAzVveqOX-w-eJoxQfoKklknJ");
    
    console.log("recruitment");
    getTable("1rN2J58uII-tg5DWYFcAxmrTAB16Egvs1QQpA6xOF");
    makeQuery("select * from 1rN2J58uII-tg5DWYFcAxmrTAB16Egvs1QQpA6xOF where StudyID = 10000");
}

gadgets.util.registerOnLoadHandler(fetchData);
